package com.ghy.ic.map.domain.mapper;

import com.ghy.ic.map.domain.model.PostgsModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * create by huweiqiang on 2018/5/21
 */
@Repository
public interface PostgsMapper {

    List<PostgsModel> getPostgsInfo();

}
