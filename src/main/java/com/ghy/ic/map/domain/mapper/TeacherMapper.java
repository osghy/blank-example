package com.ghy.ic.map.domain.mapper;

import com.ghy.ic.map.domain.model.TeacherModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * create by huweiqiang on 2018/5/18
 */
@Repository
public interface TeacherMapper {

    List<TeacherModel> getTeachers();

}
