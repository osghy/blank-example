package com.ghy.ic.map.domain.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * create by huweiqiang on 2018/5/18
 */
@Setter
@Getter
@ApiModel("教师属性")
public class TeacherModel {
    @ApiModelProperty("教师ID")
    private int id;
    @ApiModelProperty("教师名字")
    private String name;
    @ApiModelProperty("教师年龄")
    private int age;
}
