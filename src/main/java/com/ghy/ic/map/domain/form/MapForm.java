package com.ghy.ic.map.domain.form;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

/**
 * create by huweiqiang on 2018/5/22
 */
@Setter
@Getter
@ApiModel("地图表单")
public class MapForm {

    @ApiModelProperty("加载数量")
    private int count;
    @ApiModelProperty("现已加载总数量")
    private int total;
    @ApiModelProperty(hidden = true)
    private int pageIndex;
    @ApiModelProperty("检索内容")
    private String value;

    public int  getPageIndex(){
        return  this.pageIndex = total/count;
    }
}
