package com.ghy.ic.map.domain.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * create by huweiqiang on 2018/5/18
 */
@Setter
@Getter
@ApiModel("用户属性")
public class UserModel {
    @ApiModelProperty("用户ID")
    private int id;
    @ApiModelProperty("用户名字")
    private String name;
    @ApiModelProperty("用户年龄")
    private int age;
}
