package com.ghy.ic.map.domain.mapper;

import com.ghy.ic.map.domain.model.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * create by huweiqiang on 2018/5/18
 */
@Repository
public interface UserMapper {

    int insert(UserModel model);

    List<UserModel> getUsers();
}
