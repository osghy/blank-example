package com.ghy.ic.map.domain.model;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * create by huweiqiang on 2018/5/21
 */
@Setter
@Getter
@ApiModel("postgs属性")
public class PostgsModel {
    private int ogc_fid;
    private double distance;

}
