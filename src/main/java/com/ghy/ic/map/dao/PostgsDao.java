package com.ghy.ic.map.dao;

import com.ghy.ic.commons.persistence.DBSwitch;
import com.ghy.ic.map.domain.mapper.PostgsMapper;
import com.ghy.ic.map.domain.model.PostgsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * create by huweiqiang on 2018/5/21
 */
@Repository
public class PostgsDao {

    @Autowired
    PostgsMapper postgsMapper;

    @DBSwitch("postgs")
    public List<PostgsModel> getPostgsInfo() {
        return postgsMapper.getPostgsInfo();
    }

}
