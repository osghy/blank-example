package com.ghy.ic.map.dao;

import com.ghy.ic.commons.persistence.DBSwitch;
import com.ghy.ic.map.domain.mapper.TeacherMapper;
import com.ghy.ic.map.domain.model.TeacherModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * create by huweiqiang on 2018/5/18
 */
@Repository
public class TeacherDao {

    @Autowired
    TeacherMapper teacherMapper;

    @DBSwitch("test2")
    public List<TeacherModel> getTeachers(){
        return teacherMapper.getTeachers();
    }
}
