package com.ghy.ic.map.dao;

import com.ghy.ic.commons.persistence.DBSwitch;
import com.ghy.ic.map.domain.mapper.UserMapper;
import com.ghy.ic.map.domain.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * create by huweiqiang on 2018/5/18
 */
@Repository
public class UserDao {

    @Autowired
    private UserMapper userMapper;

    @DBSwitch("test")
    public List<UserModel> getUsers(){
        return userMapper.getUsers();
    }
}
