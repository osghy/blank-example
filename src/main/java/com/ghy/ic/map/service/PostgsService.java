package com.ghy.ic.map.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ghy.ic.map.dao.PostgsDao;
import com.ghy.ic.map.domain.form.MapForm;
import com.ghy.ic.map.domain.model.PostgsModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * create by huweiqiang on 2018/5/21
 */
@Api
@RestController
@RequestMapping("/postgs")
public class PostgsService {

    private static final Logger logger = LoggerFactory.getLogger(PostgsService.class);
    @Autowired
    PostgsDao postgsDao;

    @ApiOperation("test get postgs info !")
    @RequestMapping(value = "/getPostgsInfo")
    public String getPostgsInfo(@ModelAttribute MapForm mapForm){
        logger.info("起始pageIndex：:" + mapForm.getPageIndex() + ",检索内容：" + mapForm.getValue());
//        List<PostgsModel> list = postgsDao.getPostgsInfo();
//        PostgsModel postgsModel = new PostgsModel();

        ObjectMapper objectMapper = new ObjectMapper();
        ArrayNode arrayNode = objectMapper.createArrayNode();
        for(int i = 0;i < 8;i++){
            ObjectNode objectNode = objectMapper.createObjectNode();
            objectNode.put("id",mapForm.getPageIndex() + "_mapCard_" + i);
            objectNode.put("url","https://landsat.arcgis.com/arcgis/rest/services/Landsat/PS/ImageServer");
            objectNode.put("mapServerUrl","https://services.arcgisonline.com/ArcGIS/rest/services/USA_Topo_Maps/MapServer");
            objectNode.put("imagePath","../../../image/" + (i+1) + ".jpg");
            objectNode.put("title","30岁的职业规划");

            arrayNode.add(objectNode);
        }
        System.out.println(arrayNode.toString());
        return arrayNode.toString();
    }
}
