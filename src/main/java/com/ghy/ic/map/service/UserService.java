package com.ghy.ic.map.service;

import com.ghy.ic.map.dao.TeacherDao;
import com.ghy.ic.map.dao.UserDao;
import com.ghy.ic.map.domain.model.TeacherModel;
import com.ghy.ic.map.domain.model.UserModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * create by huweiqiang on 2018/5/18
 */
@Api("user相关api")
@RestController
@RequestMapping("/user")
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private UserDao userDao;
    @Autowired
    private TeacherDao teacherDao;

    @ApiOperation("get all users info testing")
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public List<UserModel> getUsers(){
        List<UserModel> list = userDao.getUsers();
        return list;
    }

    @ApiOperation("get all teachers info testing")
    @RequestMapping(value = "/getTeachers", method = RequestMethod.GET)
    public List<TeacherModel> getTeachers(){
        List<TeacherModel> list = teacherDao.getTeachers();
        return list;
    }

}
